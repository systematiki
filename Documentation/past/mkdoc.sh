#!/bin/bash

DIR=$(dirname "$PWD/$0")
cd "$DIR"
find "$DIR" -name "[^.]*.txt" -print -exec asciidoc {} \;

