#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Systematiki program.

This program is a command-line-driven module loader for Systematiki.  It
provides for running any combination of one Systematiki front-end and one
back-end.
"""

# Copyright 2007 Felix Rabe

# This file is part of Systematiki.

# Systematiki is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.

# Systematiki is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
# General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with Systematiki.  If not, see <http://www.gnu.org/licenses/>.

import imp
import sys


usage = """Usage:
  systematiki.py --back Back.End ... --front Front.End ..."""


def load_module(module_name):
    path = sys.path
    components = module_name.split(".")
    fn = []
    for x in components[:-1]:
        fn = [".".join(fn + [x])]
        path = imp.load_module(fn[0], *imp.find_module(x, path)).__path__
    x = components[-1]
    fn = [".".join(fn + [x])]
    return imp.load_module(fn[0], *imp.find_module(x, path))


def main(argv):
    try:
        assert argv[1] == "--back"
        backend_name = argv[2]
        i = argv.index("--front", 3)
        backend_options = argv[3:i]
        frontend_name = argv[i+1]
        frontend_options = argv[i+2:]
    except:
        print usage
        return 1
    
    backend_module = load_module(backend_name)
    frontend_module = load_module(frontend_name)
    
    backend_instance = backend_module.SystBack(*backend_options)
    frontend_instance = frontend_module.SystFront(*frontend_options)
    
    frontend_instance.set_back(backend_instance)
    backend_instance.set_front(frontend_instance)


if __name__ == "__main__":
    sys.exit(main(sys.argv))

